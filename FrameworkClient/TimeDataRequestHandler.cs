﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using WebSocketsDataTemplate.DTOs.Interfaces;

namespace FrameworkClient
{
    public class Measurement
    {
        public DateTime Timestamp { get; set; }

        public double Reading { get; set; }
    }

    public class Channel
    {
        public string Routing { get; set; }

        public string UoM { get; set; }

        public IEnumerable<Measurement> Measurements { get; set; }
    }

    public class TimeDataResponse : IWSDTO
    {
        public DateTime StartDateTime { get; set; }

        public DateTime StopDateTime { get; set; }

        public List<Channel> Channels { get; set; }

        public TimeDataResponse(int numberOfChannels, int numberOfMeasurements)
        {
            StartDateTime = DateTime.UtcNow.Subtract(TimeSpan.FromHours(1));
            StopDateTime = DateTime.UtcNow;

            Channels = new List<Channel>();
            for (int i = 0; i < numberOfChannels; i++)
            {
                Channels.Add(new Channel { Routing = $"Sensing.Sensor.Metric{i}", UoM = $"unit{i}", Measurements = GetMeasurements(numberOfMeasurements) });
            }
        }

        private IEnumerable<Measurement> GetMeasurements(int numberOfMeasurements)
        {
            var random = new Random();
            for (int i = 0; i < numberOfMeasurements; i++)
            {
                yield return new Measurement { Timestamp = DateTime.UtcNow, Reading = random.NextDouble() };
            }
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
