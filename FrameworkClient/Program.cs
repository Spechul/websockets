﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebSocketsConnector;
using WebSocketsDataTemplate.Commands;
using WebSocketsDataTemplate.DTOs;

namespace FrameworkClient
{
    class Program
    {
        static void Main(string[] args)
        {
            StartWebSockets().GetAwaiter().GetResult();
            Console.ReadLine();
        }

        public static async Task StartWebSockets()
        {
            WSConnector connector = new WSConnector("ws://localhost:5936/rpc");
            await connector.ConnectAsync();
            await connector.SendAsync(new WSCommand
                {Type = "TimeDataRequestHandler", Arguments = new string[] {"10", "400",}});
            await connector.ReceiveAsync<TimeDataResponse>(o =>
            {
                Console.WriteLine(o.Channels.Count);
            });
        }

        public static async Task ReceiveAsync(ClientWebSocket client)
        {

            while (true)
            {
                var buffer = new byte[1024 * 1000];

                var result = await client.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                Console.WriteLine(Encoding.UTF8.GetString(buffer, 0, result.Count));

                if (result.MessageType == WebSocketMessageType.Close)
                {
                    await client.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
                    break;
                }
            }
        }
    }
}
