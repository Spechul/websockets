﻿using System;

namespace WebSocketsConnector.Settings
{
    public static class ClientSettings
    {
        public const int MaxBufferSize = 400000;

        public const int MinBufferSize = 1;

        private static int _bufferSize = 10000;

        /// <summary>
        /// message buffer for websocket clients. note that this buffer will be applied to all clients
        /// </summary>
        public static int BufferSize
        {
            get => _bufferSize;
            set
            {
                if (value > MaxBufferSize)
                    throw new ArgumentException($"sorry, max buffer size is {MaxBufferSize}");

                if (value < MinBufferSize)
                    throw new ArgumentException($"sorry, min buffer size is {MinBufferSize}");

                _bufferSize = value;
            }
        }
    }
}