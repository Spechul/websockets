﻿using System;
using System.Diagnostics;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebSocketsConnector.Settings;

namespace WebSocketsConnector
{
    public class WSConnector : IDisposable
    {
        #region Fields

        private readonly string _connectionPath;

        private ClientWebSocket _clientWebSocket = new ClientWebSocket();

        private CancellationTokenSource _handlerTerminator;

        private CancellationTokenSource _reconnectTerminator;

        #endregion

        #region Constructors

        /// <param name="connectionString">i.e. ws://host:port/endpoint</param>
        public WSConnector(string connectionString)
        {
            _connectionPath = connectionString;
        }

        #endregion

        #region Properties

        public bool IsSocketOpen => _clientWebSocket.State == WebSocketState.Open;

        #endregion

        #region Public Methods

        public async Task ConnectAsync()
        {
            await _clientWebSocket.ConnectAsync(new Uri(_connectionPath), CancellationToken.None);

            if (_reconnectTerminator == null)
            {
                _ = Task.Run(() => // reconnector
                {
                    Thread.Sleep(5000); // first connection timeout
                    while (!_reconnectTerminator.IsCancellationRequested)
                    {
                        lock (_clientWebSocket)
                        {
                            if (!IsSocketOpen)
                            {
                                ReconnectAsync().Wait();
                            }
                        }

                        Thread.Sleep(5000); 
                    }
                });
            }
        }

        public async Task CloseAsync(WebSocketCloseStatus status, string description, CancellationToken token)
        {
            await _clientWebSocket.CloseAsync(status, description, token);
        }

        public async Task CloseAsync(string description)
        {
            await CloseAsync(WebSocketCloseStatus.NormalClosure, description, CancellationToken.None);
        }

        public async Task CloseAsync()
        {
            await CloseAsync(WebSocketCloseStatus.NormalClosure, "closed manually", CancellationToken.None);
        }

        public async Task ReconnectAsync()
        {
            _clientWebSocket.Dispose();

            _clientWebSocket = new ClientWebSocket();

            await ConnectAsync();
        }

        public async Task SendAsync<T>(T obj)
        {
            lock(_clientWebSocket)
            {
                if (!IsSocketOpen)
                {
                    ReconnectAsync().Wait();
                }
            }

            var str = JsonConvert.SerializeObject(obj);
            var buffer = Encoding.UTF8.GetBytes(str);
            await _clientWebSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Text, true,
                CancellationToken.None);
        }

        public async Task ReceiveAsync<T>(Action<T> handler)
        {

            T obj = await ReceiveMessageOrDefault<T>();

            try
            {
                handler(obj);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void RunHandler<T>(Action<T> handler)
        {
            _handlerTerminator?.Cancel();
            _handlerTerminator = new CancellationTokenSource();
            var token = _handlerTerminator.Token;

            Task.Run(async () =>
            {
                while (!token.IsCancellationRequested)
                {
                    var res = await ReceiveMessageOrDefault<T>();
                    try
                    {
                        handler(res);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e);
                    }
                }
            }, token);
        }

        public void TerminateHandler()
        {
            _handlerTerminator?.Cancel();
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            _reconnectTerminator.Cancel();
            _handlerTerminator?.Cancel();
            _clientWebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "closed by disposer", CancellationToken.None);
        }

        #endregion

        #region Private Methods

        private async Task<T> ReceiveMessageOrDefault<T>()
        {
            var builder = new StringBuilder();
            WebSocketReceiveResult result;
            do
            {
                var buffer = new byte[1024 * ClientSettings.BufferSize];
                result = await _clientWebSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                builder.Append(Encoding.UTF8.GetString(buffer, 0, result.Count));
            } while (!result.EndOfMessage);

            var str = builder.ToString();

            T obj;
            try
            {
                obj = JsonConvert.DeserializeObject<T>(str);
                return obj;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return default;
            }
        }

        #endregion
    }
}
