﻿namespace WebSocketsDataTemplate.Commands
{
    public class WSCommand
    {
        public string Type { get; set; }

        public string[] Arguments { get; set; }

        public WSCommand()
        {
            
        }

        public WSCommand(string[] arguments)
        {
            Arguments = arguments;
            Type = "";
        }

        public WSCommand(string type, string[] arguments)
        {
            Type = type;
            Arguments = arguments;
        }
    }
}