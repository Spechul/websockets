﻿namespace WebSocketsDataTemplate.DTOs.Interfaces
{
    public interface IWSDTO
    {
        string ToJson();
    }
}