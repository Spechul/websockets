﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebSocketsCommunication.Attributes;
using WebSocketsCommunication.SocketsManager;
using WebSocketsDataTemplate.Commands;
using WebSocketsDataTemplate.DTOs.Interfaces;

namespace PureWebSockets.Managers.Handlers
{
    public class Measurement
    {
        public DateTime Timestamp { get; set; }

        public double Reading { get; set; }
    }

    public class Channel
    {
        public string Routing { get; set; }

        public string UoM { get; set; }

        public IEnumerable<Measurement> Measurements { get; set; }
    }

    public class TimeDataResponse : IWSDTO
    {
        public DateTime StartDateTime { get; set; }

        public DateTime StopDateTime { get; set; }

        public List<Channel> Channels { get; set; }

        public TimeDataResponse(int numberOfChannels, int numberOfMeasurements)
        {
            StartDateTime = DateTime.UtcNow.Subtract(TimeSpan.FromHours(1));
            StopDateTime = DateTime.UtcNow;

            Channels = new List<Channel>();
            for (int i = 0; i < numberOfChannels; i++)
            {
                Channels.Add(new Channel { Routing = $"Sensing.Sensor.Metric{i}", UoM = $"unit{i}", Measurements = GetMeasurements(numberOfMeasurements) });
            }
        }

        private IEnumerable<Measurement> GetMeasurements(int numberOfMeasurements)
        {
            var random = new Random();
            for (int i = 0; i < numberOfMeasurements; i++)
            {
                yield return new Measurement { Timestamp = DateTime.UtcNow, Reading = random.NextDouble() };
            }
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    [BelongsTo(typeof(RPCManager))]
    public class TimeDataRequestHandler : WSCommandHandler
    {
        public override async Task<IWSDTO> FormAnswer(WSCommand command)
        {
            int numberOfChannels = Convert.ToInt32(command.Arguments[0]);
            int numberOfMeasurements = Convert.ToInt32(command.Arguments[1]);
            return new TimeDataResponse(numberOfChannels, numberOfMeasurements);
        }
    }
}
