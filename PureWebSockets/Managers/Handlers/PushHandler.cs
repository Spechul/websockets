﻿using System;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using WebSocketsCommunication.SocketsManager;
using WebSocketsDataTemplate.Commands;
using WebSocketsDataTemplate.DTOs.Interfaces;

namespace PureWebSockets.Managers.Handlers
{
    public class PushHandler : WSCommandInvoker
    {
        private ConcurrentDictionary<string, CancellationTokenSource> _terminators = new ConcurrentDictionary<string, CancellationTokenSource>();

        public override Task<IWSDTO> FormAnswer(WSCommand command)
        {
            throw new System.NotImplementedException();
        }

        public override async Task InvokeAsync(WSCommand command, WebSocket socket, SocketHandler callerHandler)
        {
            var channels = Convert.ToInt32(command.Arguments[1]);
            var measurements = Convert.ToInt32(command.Arguments[2]);
            var parts = Convert.ToInt32(command.Arguments[3]);
            switch (command.Arguments[0])
            {
                case "start":
                    {
                        var cts = new CancellationTokenSource();
                        var token = cts.Token;
                        var key = callerHandler.Connections.GetId(socket);
                        if (_terminators.ContainsKey(key))
                        {
                            return;
                        }
                        _terminators.TryAdd(key, cts);
                        _ = Task.Run(async () =>
                        {
                            int i = 0;
                            int n = Convert.ToInt32(command.Arguments[1]);
                            while (i < n && !token.IsCancellationRequested)
                            {
                                await callerHandler.SendMessage(socket, GenerateResponse(channels, measurements).ToJson());
                                Thread.Sleep(50);
                            }
                        }, token);
                    }
                    break;

                case "stop":
                    {
                        _terminators.TryRemove(callerHandler.Connections.GetId(socket), out var term);
                        term.Cancel();
                    }
                    break;
            }
        }

        private IWSDTO GenerateResponse(int channels, int measurements)
        {
            return new TimeDataResponse(channels, measurements);
        }
    }
}