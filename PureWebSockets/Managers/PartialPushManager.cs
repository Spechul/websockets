﻿using System.Net.WebSockets;
using System.Threading.Tasks;
using WebSocketsCommunication.DataHelpers;
using WebSocketsCommunication.SocketsManager;
using WebSocketsDataTemplate.Commands;

namespace PureWebSockets.Managers
{
    public class PartialPushManager : WSCommandInvokerManager
    {
        public PartialPushManager(ConnectionManager connections) : base(connections)
        {
        }

        public override async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            var command = ByteProcessor.TryProcess<WSCommand>(buffer);
            _handlers.TryGetValue(command.Type, out var handler);
            await handler?.InvokeAsync(command, socket, this);
        }
    }
}