﻿using System.Net.WebSockets;
using System.Threading.Tasks;
using WebSocketsCommunication.DataHelpers;
using WebSocketsCommunication.SocketsManager;
using WebSocketsDataTemplate.Commands;
using WebSocketsDataTemplate.DTOs.Interfaces;

namespace PureWebSockets.Managers
{
    public class RPCManager : WSCommandManager
    {
        private WSCommandManager _wsCommandManagerImplementation;

        public RPCManager(ConnectionManager connections) : base(connections)
        {

        }

        public override async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            var command = ByteProcessor.TryProcess<WSCommand>(buffer);
            _handlers.TryGetValue(command.Type, out var handler);
            IWSDTO dto = await handler?.FormAnswer(command);
            await SendMessage(socket, dto.ToJson());
        }
    }
}