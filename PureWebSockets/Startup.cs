﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using PureWebSockets.Managers;
using WebSocketsCommunication.SocketsManager;

namespace PureWebSockets
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddWebSocketManager();

            services.AddMvc();

            // only for browser clients that ain't allow cross origin requests. C# would work without cors
            services.AddCors(options => options.AddPolicy("CorsPolicy",
                builder =>
                {
                    builder.AllowAnyMethod().AllowAnyHeader()
                        .WithOrigins("http://localhost:5936")
                        .AllowCredentials();
                }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc(rt => { rt.MapRoute("default", "{controller=Home}/{action=Index}/{id?}"); });

            app.UseWebSockets();

            app.MapSockets(
                "/rpc", serviceProvider.GetService<RPCManager>());

            /*app.MapSockets(
                "/ws", serviceProvider.GetService<PushDataHandler>());

            app.MapSockets(
                "/partial", serviceProvider.GetService<PartialPushHandler>());

            app.MapSockets(
                "/rpc", serviceProvider.GetService<RPCHandler>());*/

            app.UseCors("CorsPolicy");
            app.UseStaticFiles();
        }
    }
}
