// a command for hub

let command0 = {
    Type: "BroadcastEcho",
    Arguments: []
}

let command1 = {
    Type: "SendFrame",
    Arguments: [
        "start",
        "20" // pass here "stop" to stop pushing via rpc
    ]
}

let command2 = {
    Type: "InterruptableSend",
    Arguments: [
        "start",
        "20" // pass here "stop" to stop pushing via rpc
    ]
}

let command3 = {
    Type: "InterruptableSend",
    Arguments: [
        "stop"
        // pass here "stop" to stop pushing via rpc
    ]
}

let command4 = { Type: "TimeDataRequestHandler", Arguments: ["10", "3600"] };

let webSocket = new WebSocket("ws://192.168.1.252:5936/rpc"); // to use rpc.
//let webSocket = new WebSocket("ws://localhost:5936/ws"); // to just push
//let webSocket = new WebSocket("ws://localhost:5936/partial");
//let webSocket = new WebSocket("ws://albacore2server20190628093201.azurewebsites.net/partial/");
//let webSocket = new WebSocket('ws://echo.websocket.org');

webSocket.onmessage = function receive(event) {
    console.log(event.data);
    console.log(event.data.length);
};

webSocket.onopen = function send() {
    this.send(JSON.stringify(command4)); //uncomment this to send command
};

webSocket.onclose = function close() {
    alert("closed");
}

webSocket.onerror = function error(err) {
    alert(err);
}