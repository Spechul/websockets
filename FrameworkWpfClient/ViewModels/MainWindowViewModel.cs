﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using FrameworkWpfClient.Commands.WinAdminClientCore.RelayCommand;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using WebSocketsConnector;
using WebSocketsDataTemplate.Commands;
using WebSocketsDataTemplate.DTOs;

namespace FrameworkWpfClient.ViewModels
{
    public class MainWindowViewModel : ViewModelBase, IDisposable
    {
        #region Fields

        private int _pageSize = 10;

        private SeriesCollection _series;

        private WSConnector _connector = new WSConnector("ws://localhost:5936/dispatcher");

        private ICommand _startReceive;

        private ICommand _stopReceive;

        private CancellationTokenSource _cancelReceive;

        #endregion

        #region Properties

        public SeriesCollection Series
        {
            get => _series;
            set
            {
                _series = value;
                OnPropertyChanged();
            }
        }

        public int PageSize
        {
            get => _pageSize;
            set
            {
                _pageSize = value;
                OnPropertyChanged();
            }
        }

        public ICommand StartReceive =>
            _startReceive ?? (_startReceive = new RelayCommand(p => true,
                p =>
                {
                    Task.Run(async () => await _connector.SendAsync(new WSCommand() { Type = "SendFrame", Arguments = new string[] { "start", PageSize.ToString() } }));
                }));

        public ICommand StopReceive =>
            _stopReceive ?? (_stopReceive = new RelayCommand(p => true,
                p =>
                {
                    Task.Run(async () =>
                    {
                        await _connector.SendAsync(new WSCommand
                        { Type = "SendFrame", Arguments = new string[] { "stop" } });
                    });
                }));

        #endregion

        #region Constructor

        public MainWindowViewModel()
        {
            Series = new SeriesCollection()
            {
                new LineSeries() { Values = new ChartValues<ObservableValue>() }
            };
            for (int i = 0; i < 10; i++)
            {
                Series[0].Values.Add(new ObservableValue(i));
            }

            Task.Run(async () =>
            {
                await _connector.ConnectAsync();
                //await _connector.SendAsync(new WSCommand("SendFrame", new string[] { "start" }));
            }).Wait();//.GetAwaiter().GetResult();

            _cancelReceive = new CancellationTokenSource();
            var token = _cancelReceive.Token;

            Task.Run(async () =>
            {
                while (!token.IsCancellationRequested)
                {
                    await _connector.ReceiveAsync<DataFrameDTO>(dataframe =>
                    {
                        foreach (var dot in dataframe.Dots["depth"])
                        {
                            Series[0].Values.Add(new ObservableValue(dot));
                        }

                        var cnt = Series[0].Values.Count;
                        if (cnt > PageSize * 5)
                        {
                            for (int i = 0; i < PageSize; i++)
                                Series[0].Values.RemoveAt(0);
                        }
                    });
                }
            }, token);
        }

        #endregion

        public void Dispose()
        {
            _cancelReceive.Cancel();
            _connector.Dispose();
        }
    }
}