﻿using System.Windows;
using FrameworkWpfClient.ViewModels;

namespace FrameworkWpfClient
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private MainWindowViewModel vm = new MainWindowViewModel();

        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            var mainWindow = new MainWindow();
            
            mainWindow.DataContext = vm;
            mainWindow.Show();
        }

        private void App_OnExit(object sender, ExitEventArgs e)
        {
            vm.Dispose();
        }
    }
}
