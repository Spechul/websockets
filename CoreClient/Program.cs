﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebSocketsCommunication.Settings;
using WebSocketsConnector;
using WebSocketsDataTemplate.Commands;
using WebSocketsDataTemplate.DTOs;

namespace CoreClient
{
    class Program
    {
        private const string WsConnectionString = "ws://localhost:5936/dispatcher";

        static async Task Main(string[] args)
        {
            WSConnector connector = new WSConnector("ws://localhost:5936/dispatcher");
            await connector.ConnectAsync();
            connector.RunHandler<DataFrameDTO>(dto =>
            {
                foreach(var dot in dto.Dots["depth"])
                {
                    Console.WriteLine(dot);
                }
            });
            await connector.SendAsync(new WSCommand()
                {Type = "SendFrame", Arguments = new string[] {"start", "20"}});
            Console.ReadLine();
            await connector.CloseAsync();

            // uncomment to use just websockets
            //StartWebSockets().GetAwaiter().GetResult();
        }

        public static async Task StartWebSockets()
        {
            var client = new ClientWebSocket();
            await client.ConnectAsync(new Uri(WsConnectionString), CancellationToken.None);
            _ = Task.Run(() =>
              {
                  while (true)
                  {
                      if (client.State == WebSocketState.CloseReceived || client.State == WebSocketState.CloseSent)
                          return;

                      if (client.State == WebSocketState.Closed)
                      {
                          var reconnect = client.ConnectAsync(new Uri(WsConnectionString), CancellationToken.None);
                          Task.WhenAll(reconnect).GetAwaiter().GetResult();
                      }

                      Thread.Sleep(250);
                  }
              });

            Console.WriteLine("connected");
            Console.WriteLine("type something. empty string is for exit");



            var send = Task.Run(async () =>
            {
                string message;

                while ((message = Console.ReadLine()) != null && message != string.Empty)
                {
                    var buffer = Encoding.UTF8.GetBytes(message);

                    var str = JsonConvert.SerializeObject(new WSCommand(null)
                        {Type = CommandType.BroadcastEcho, Arguments = new[]
                        {
                            JsonConvert.SerializeObject(new TerminateDTO() {RequestId = "1"})
                        }});

                    buffer = Encoding.UTF8.GetBytes(str);
                    client.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Text, false, CancellationToken.None);
                }

                await client.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
            });

            var receive = ReceiveAsync(client);

            await Task.WhenAll(send, receive);
        }

        public static async Task ReceiveAsync(ClientWebSocket client)
        {
            
            while (client.State != WebSocketState.CloseReceived)
            {
                var buffer = new byte[1024 * WSSettings.BufferSize];

                var result = await client.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                
                try
                {
                    var res = Encoding.UTF8.GetString(buffer).TrimEnd('\0');
                    
                    Console.WriteLine(res);
                    //var got = JsonConvert.DeserializeObject<TerminatedDTO>(res);
                    var got = JsonConvert.DeserializeObject<List<ChannelDTO>>(res);
                    //Console.WriteLine(got.Message);
                    Console.WriteLine(got.Count);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }

                if (result.MessageType == WebSocketMessageType.Close)
                {
                    await client.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
                    break;
                }
            }
        }
    }
}
