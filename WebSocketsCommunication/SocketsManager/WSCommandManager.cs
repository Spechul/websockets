﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Reflection;
using System.Threading.Tasks;
using WebSocketsCommunication.Attributes;

namespace WebSocketsCommunication.SocketsManager
{
    public abstract class WSCommandManager : SocketHandler
    {
        protected ConcurrentDictionary<string, WSCommandHandler> _handlers = new ConcurrentDictionary<string, WSCommandHandler>();

        public WSCommandManager(ConnectionManager connections) : base(connections)
        {
            //LoadFromAssembly(Assembly.Load("WebSocketsCommunication"));
            LoadFromAssembly(Assembly.GetEntryAssembly());
        }

        private void LoadFromAssembly(Assembly assembly)
        {
            foreach (var type in assembly.ExportedTypes)
            {

                if (type.BaseType == typeof(WSCommandHandler) && type != typeof(WSCommandInvoker))
                {
                    var attr =
                        type.GetCustomAttributes(typeof(BelongsToAttribute), true).FirstOrDefault() as
                            BelongsToAttribute;
                    if (attr?.Type != GetType())
                        continue;
                    var handler = (WSCommandHandler)Activator.CreateInstance(type);
                    _handlers.TryAdd(type.Name.Split(".").Last(), handler);
                }
            }
        }
    }
}