﻿using System;
using System.Diagnostics;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WebSocketsCommunication.Settings;

namespace WebSocketsCommunication.SocketsManager
{
    public class SocketMiddleware
    {
        private readonly RequestDelegate _next;

        private SocketHandler Handler { get; set; }


        public SocketMiddleware(RequestDelegate next, SocketHandler handler)
        {
            _next = next;
            Handler = handler;
        }

        public SocketMiddleware()
        {
            
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest)
                return;

            var socket = await context.WebSockets.AcceptWebSocketAsync();

            await Handler.OnConnected(socket);

            await Receive(socket, async (result, buffer) =>
            {
                if (result.MessageType == WebSocketMessageType.Text)
                {
                    try
                    {
                        await Handler.ReceiveAsync(socket, result, buffer);
                    }
                    catch (Exception e)
                    {
                        // organize logging for failed handlers here or make sure you cached everything inside them
                        Debug.WriteLine(e);
                    }
                }
                else if(result.MessageType == WebSocketMessageType.Close)
                {
                    try
                    {
                        await Handler.OnDisconnected(socket);
                    }
                    catch (WebSocketException e)
                    {
                        // add logging for aborted connections here
                        Debug.WriteLine(e);
                    }
                    
                }
            });
        }

        private async Task Receive(WebSocket socket, Action<WebSocketReceiveResult, byte[]> messageHandler)
        {
            while (socket.State == WebSocketState.Open)
            {
                var buffer = new byte[1024 * WSSettings.BufferSize];

                var result = await socket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                messageHandler(result, buffer);
            }
        }
    }
}