﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace WebSocketsCommunication.SocketsManager
{
    public class ConnectionManager : IDisposable
    {
        private ConcurrentDictionary<string, WebSocket> _connections = new ConcurrentDictionary<string, WebSocket>();

        private ConcurrentDictionary<string, WSCommandHandler> _handlers = new ConcurrentDictionary<string, WSCommandHandler>();

        private CancellationTokenSource checkTerminator = new CancellationTokenSource();

        #region Constructor

        public ConnectionManager()
        {
            Task.Run(() =>
            {
                foreach (var key in _connections.Keys)
                {
                    var con = _connections[key];
                    if (con.State == WebSocketState.Aborted || con.State == WebSocketState.Closed)
                    {
                        con.Dispose();
                        _connections.TryRemove(key, out _);
                    }
                }
                Thread.Sleep(15050); // 15 seconds is default keepalive interval
            }, checkTerminator.Token);
        }

        #endregion

        #region Public Methods

        public WebSocket GetSocketById(string id)
        {
            return _connections.FirstOrDefault(x => x.Key == id).Value;
        }

        public ConcurrentDictionary<string, WebSocket> GetAllConnections()
        {
            return _connections;
        }

        public string GetId(WebSocket socket)
        {
            return _connections.FirstOrDefault(x => x.Value == socket).Key;
        }

        public async Task RemoveSocketAsync(string id)
        {
            if (_connections.TryRemove(id, out var socket))
                await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "closed by RemoveAsync method", CancellationToken.None);
        }

        public bool AddSocket(WebSocket socket)
        {
            return _connections.TryAdd(GetConnectionId(), socket);
        }

        public bool AddHandler(string method, WSCommandHandler handler)
        {
            return _handlers.TryAdd(method, handler);
        }

        public ConcurrentDictionary<string, WSCommandHandler> GetHandlers() => _handlers;

        #endregion

        private string GetConnectionId()
        {
            return Guid.NewGuid().ToString("N");
        }

        public void Dispose()
        {
            checkTerminator.Cancel();
            foreach (var val in GetAllConnections().Values)
            {
                val.Dispose();
            }
        }
    }
}
