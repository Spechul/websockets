﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebSocketsCommunication.SocketsManager
{
    public abstract class SocketHandler
    {

        public ConnectionManager Connections { get; set; }

        protected ConcurrentDictionary<string, CancellationTokenSource> TaskTerminators { get; set; } 
            = new ConcurrentDictionary<string, CancellationTokenSource>();

        public SocketHandler(ConnectionManager connections)
        {
            Connections = connections;
        }

        public virtual async Task OnConnected(WebSocket socket)
        {
            await Task.Run(() =>
            {
                Connections.AddSocket(socket);
            });
        }

        public virtual async Task OnDisconnected(WebSocket socket)
        {
            var id = Connections.GetId(socket);
            CancellationTokenSource cts;
            TaskTerminators.TryGetValue(id, out cts);
            cts?.Cancel();
            await Connections.RemoveSocketAsync(id);
        }

        public async Task SendMessage(WebSocket socket, string message)
        {
            if (socket.State != WebSocketState.Open)
                return;

            ReadOnlyMemory<byte> bytes = new ReadOnlyMemory<byte>(Encoding.UTF8.GetBytes(message));

            await socket.SendAsync(bytes, WebSocketMessageType.Text,
                true, CancellationToken.None);
        }

        /// <summary>
        /// Registers a handler task for a socket.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="handler"></param>
        /// <returns></returns>
        public bool Subscribe(WebSocket socket, Action<WebSocket> handler)
        {
            try
            {
                var cts = new CancellationTokenSource();
                var id = Connections.GetId(socket);
                Task.Run(() =>
                {
                    while (!cts.IsCancellationRequested)
                    {
                        handler(socket);
                        Thread.Sleep(250);
                    }
                }, cts.Token);

                TaskTerminators[id] = cts;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Subscribe(string id, Action<WebSocket> handler)
        {
            return Subscribe(Connections.GetSocketById(id), handler);
        }

        public bool Unsubscribe(string id)
        {
            try
            {
                TaskTerminators.FirstOrDefault(x => x.Key == id).Value.Cancel();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Unsubscribe(WebSocket socket)
        {
            try
            {
                var id = Connections.GetId(socket);
                TaskTerminators[id].Cancel();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            
        }

        public async Task SendMessage(string id, string message)
        {
            await SendMessage(Connections.GetSocketById(id), message);
        }

        public async Task SendMessageToAll(string message)
        {
            foreach (var connection in Connections.GetAllConnections())
            {
                await SendMessage(connection.Value, message);
            }
        }

        public abstract Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer);
    }
}