﻿using System.Net.WebSockets;
using System.Threading.Tasks;
using WebSocketsDataTemplate.Commands;

namespace WebSocketsCommunication.SocketsManager
{
    public abstract class WSCommandInvoker : WSCommandHandler
    {
        /// <summary>
        /// use only for running background tasks
        /// </summary>
        /// <param name="command"></param>
        /// <param name="socket"></param>
        /// <param name="callerHandler"></param>
        /// <returns></returns>
        public abstract Task InvokeAsync(WSCommand command, WebSocket socket, SocketHandler callerHandler);
    }
}