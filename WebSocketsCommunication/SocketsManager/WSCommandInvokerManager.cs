﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Reflection;
using System.Threading.Tasks;
using WebSocketsCommunication.Attributes;

namespace WebSocketsCommunication.SocketsManager
{
    public abstract class WSCommandInvokerManager : SocketHandler
    {
        protected ConcurrentDictionary<string, WSCommandInvoker> _handlers = new ConcurrentDictionary<string, WSCommandInvoker>();

        public WSCommandInvokerManager(ConnectionManager connections) : base(connections)
        {
            LoadFromAssembly(Assembly.GetEntryAssembly());
            //LoadFromAssembly(Assembly.Load("WebSocketsCommunication"));
        }

        public override async Task OnConnected(WebSocket socket)
        {
            Connections.AddSocket(socket);
        }

        private void LoadFromAssembly(Assembly assembly)
        {
            foreach (var type in assembly.ExportedTypes)
            {

                if (type.BaseType == typeof(WSCommandInvoker))
                {
                    var attr =
                        type.GetCustomAttributes(typeof(BelongsToAttribute), true).FirstOrDefault() as
                            BelongsToAttribute;
                    if (attr?.Type != GetType())
                        continue;
                    var handler = (WSCommandInvoker)Activator.CreateInstance(type);
                    _handlers.TryAdd(type.Name.Split(".").Last(), handler);
                }
            }
        }
    }
}