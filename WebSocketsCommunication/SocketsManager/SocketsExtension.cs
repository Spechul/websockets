﻿using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace WebSocketsCommunication.SocketsManager
{
    public static class SocketsExtension
    {
        public static IServiceCollection AddWebSocketManager(this IServiceCollection services)
        {
            services.AddSingleton<ConnectionManager>();

            //LoadFromAssembly(services, Assembly.Load("WebSocketsCommunication"));
            LoadFromAssembly(services, Assembly.GetEntryAssembly());

            return services;
        }

        private static void LoadFromAssembly(IServiceCollection services, Assembly assembly)
        {
            foreach (var type in assembly.ExportedTypes)
            {
                if (type.BaseType == typeof(SocketHandler) && type != typeof(WSCommandManager) && type != typeof(WSCommandInvoker)
                    && type != typeof(WSCommandInvokerManager))
                {
                    services.AddScoped(type);
                }

                if (type.BaseType == typeof(WSCommandManager))
                {
                    services.AddScoped(type);
                }

                if (type.BaseType == typeof(WSCommandInvoker))
                {
                    services.AddScoped(type);
                }

                if (type.BaseType == typeof(WSCommandInvokerManager))
                {
                    services.AddScoped(type);
                }
            }
        }

        public static IApplicationBuilder MapSockets(this IApplicationBuilder app, PathString path, SocketHandler handler)
        {
            return app.Map(path, x =>
            {
                x.UseMiddleware<SocketMiddleware>(handler);
            });
        }
    }
}