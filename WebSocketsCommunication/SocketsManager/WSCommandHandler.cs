﻿using System.Net.WebSockets;
using System.Threading.Tasks;
using WebSocketsDataTemplate.Commands;
using WebSocketsDataTemplate.DTOs;
using WebSocketsDataTemplate.DTOs.Interfaces;

namespace WebSocketsCommunication.SocketsManager
{
    public abstract class WSCommandHandler
    {
        /// <summary>
        /// use to generate dto
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public abstract Task<IWSDTO> FormAnswer(WSCommand command);
    }
}