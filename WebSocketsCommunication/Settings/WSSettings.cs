﻿using System;

namespace WebSocketsCommunication.Settings
{
    public static class WSSettings
    {
        public const int MaxBufferSize = 400000;

        public const int MinBufferSize = 1;

        private static int _bufferSize = 10000;

        public static int BufferSize
        {
            get => _bufferSize;
            set
            {
                if (value > MaxBufferSize)
                    throw new ArgumentException($"sorry, max buffer size is {MaxBufferSize}");

                if (value < MinBufferSize)
                    throw new ArgumentException($"sorry, min buffer size is {MinBufferSize}");

                _bufferSize = value;
            }
        }
    }
}