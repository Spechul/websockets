﻿using System;

namespace WebSocketsCommunication.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class BelongsToAttribute : Attribute
    {
        public Type Type;

        public BelongsToAttribute(Type type)
        {
            Type = type;
        }
    }
}