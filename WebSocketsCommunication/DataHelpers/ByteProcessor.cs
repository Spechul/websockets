﻿using System;
using System.Diagnostics;
using System.Text;
using Newtonsoft.Json;

namespace WebSocketsCommunication.DataHelpers
{
    public class ByteProcessor
    {
        public static T Process<T>(byte[] bytes)
        {
            string str = Encoding.UTF8.GetString(bytes).TrimEnd('\0');
            T t = JsonConvert.DeserializeObject<T>(str);
            return t;
        }

        public static T TryProcess<T>(byte[] bytes)
        {
            try
            {
                string str = Encoding.UTF8.GetString(bytes).TrimEnd('\0');
                T t = JsonConvert.DeserializeObject<T>(str);
                return t;
            }
            catch (Exception e)
            {
                Debug.WriteLine($"failed to deserialize incoming json object {e.Message}");
                return default;
            }
        }
    }
}